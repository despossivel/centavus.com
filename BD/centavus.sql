CREATE SCHEMA `centavus` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `centavus`.`preCadastro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NOT NULL,
  `endereco` VARCHAR(255) NULL,
  `genero` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
