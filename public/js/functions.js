let notification = (msg)=>{
    $('.hide').show();  
    if(msg){
      $('.containerNotification').html(msg);
      $('.cadastreSeFrom').hide();
    }else{
      $('.containerNotification').empty();
    }
  }
 
  let cadastreSe = ()=>{
    notification();
    $('.cadastreSeFrom').show();
    $('.containerNotification').empty();
  }

  let clearNotification = (hide, empty)=>{
    $(hide).hide();
    $(empty).empty();
  }


 let setRemoveNotification = async (hide,empty,className)=>{
    await setTimeout(()=> { 
                    clearNotification(hide,empty);
                    $(empty).removeClass(className) 
                  return true;
                },3000);
  }



let prosseguir = () =>{
    let nome = $('#nome').val();
    let email = $('#email').val();
    let telefone = $('#telefone').val();
    let endereco = $('#endereco').val();
    let genero = $('#genero').val();
  
      if(nome !== "" && email !== "" && genero !== ""){
        let dados = {nome,email,telefone,endereco,genero};

        cadastro(dados).then(data=>{ 
            let json = JSON.parse(data); 
              if(json.error){
  
                $('.notificationFormulario').show();
                $('#msgFormulario').html(json.msg).addClass('notification is-danger is-rounded');

                  setRemoveNotification('.notificationFormulario','#msgFormulario','notification is-danger is-rounded');
                  return;
              }
  
              $('.notificationFormulario').show();
              $('#msgFormulario').html(json.msg).addClass('notification is-success is-rounded');
            
                setRemoveNotification('.notificationFormulario','#msgFormulario','notification is-success is-rounded').then(resolve=>{
                    location.href = 'index.html';
                });

        });
  

      }else{
        $('.notificationFormulario').show();
        $('#msgFormulario').html('Preencha os campos marados com *, eles são obrigatórios.').addClass('notification is-danger is-rounded');
  
        setRemoveNotification('.notificationFormulario','#msgFormulario','notification is-danger is-rounded')
      }
  
  } 
  