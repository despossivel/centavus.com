<?php
header("Access-Control-Allow-Origin: *");

include '../config/conexao.php';

$connection = new createConnect();
$connection->connect();
$resposta = array();

$dados = $_POST;

$nome = $dados['nome'];
$email = $dados['email'];
$telefone = $dados['telefone'];
$endereco = $dados['endereco'];
$genero = $dados['genero'];

$queryCheckEmail = 'SELECT email FROM `preCadastro` WHERE email="' . $email . '" ';
$checkEmail = mysqli_query($connection->conn, $queryCheckEmail);
$numrows = mysqli_num_rows($checkEmail);

if ($numrows > 0) {
    $resposta['error'] = true;
    $resposta['msg'] = 'Email já cadastrado!';
} else {

    $query = 'INSERT INTO `preCadastro` (nome, email,telefone,endereco,genero) VALUES ("' . $nome . '","' . $email . '","' . $telefone . '","' . $endereco . '","' . $genero . '")';
    $result = mysqli_query($connection->conn, $query);
    if ($result) {

        $resposta['success'] = true;
        $resposta['msg'] = 'Pré-cadastro efetuado com sucesso!';
    } else {

        $resposta['error'] = true;
        $resposta['msg'] = 'Ops! Aconteceu alguma coisa.';
    }
}
echo json_encode($resposta);
